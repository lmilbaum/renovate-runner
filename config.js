module.exports = {
  platform: 'gitlab',
  endpoint: 'https://gitlab.com/api/v4/',
  onboardingConfig: {
    "extends": [
      "github>platform-engineering-org/.github"
    ]
  },
  repositories: [
    'centos/automotive/platform-engineering/renovate-runner',
    'platform-engineering-org/gitlab-ci',
    'bootc-org/renovate-config',
    'bootc-org/centos-bootc-layered',
    'bootc-org/examples',
    'bootc-org/tests/container-fixtures',
    'bootc-org/platform-engineering/testing-framework/terraform-ec2-instance-module',
    'bootc-org/platform-engineering/testing-framework/provisioning-bootc',
    'bootc-org/platform-engineering/testing-framework/terraform-aws-bootstrap-bib-module'
  ],
  hostRules: [
    {
      matchHost: 'github.com',
      token: process.env.GITHUB_COM_TOKEN,
    }
  ]
};
