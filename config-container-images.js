module.exports = {
  platform: 'gitlab',
  endpoint: 'https://gitlab.com/api/v4/',
  onboardingConfig: {
      "extends": [
          "github>platform-engineering-org/.github"
      ]
  },
  hostRules: [
    {
      matchHost: 'github.com',
      token: process.env.GITHUB_COM_TOKEN,
    }
  ],

  autodiscover: true,
  autodiscoverFilter: ["centos/automotive/container-images/*"]
};
